# hoja de trabajo 2

Escriba un programa que maneje las diferentes instrucciones de manejo
de archivos, utilizando manejo de excepciones.
✓ Fopen
✓ Fclose
✓ Fgetc
✓ Fputc
✓ Feof
✓ Rewind
✓ Fgets
✓ Fputs
✓ Fread
✓ Fwrite
✓ Fprintf
✓ Fscanf